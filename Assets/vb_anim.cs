﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class vb_anim : MonoBehaviour, IVirtualButtonEventHandler
{
    public GameObject vBtnObj;
    public Animator chessPieces;

    // Start is called before the first frame update
    void Start()
    {
        vBtnObj = GameObject.Find("KingBtn");
        vBtnObj.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        chessPieces.GetComponent<Animator>();
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        chessPieces.Play("king_animation");
        Debug.Log("BTN PRESSED");
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        chessPieces.Play("none");
        Debug.Log("BTN RELEASED");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
